<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title>Documento sin título</title>

 <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
	<div class='container'>
<p><h3><strong>DOCUMENTACIÓN API INEGI PIB</strong></h3></p>
<p><strong>INTEGRANTES:</strong></p>
<p>-López Ojeda Christina<br />
-Mendiola Sarabio Bernardo<br />
-Pinacho García Gibran Eleacid<br />
-Morales Lorenzo Agustín</p>

<div class='bg-info'>
<p><center class='bg-primary'>MÉTODOS GET</center></p>

<p><a href="/api/entidades">/api/entidades</a>;: Devuelve todas las entidades </p>
<p><a href="/api/entidades/0">/api/entidades/$id</a>;: Mostrar la entidad con el id especificado, sustituir id por el deseado</p>
<p><a href="/api/municipios">/api/municipios</a>;: Mostrar todos los municipios</p>
<p><a href="/api/municipios/0">/api/municipios/$id</a>&quot;: Mostrar el municipio con el id especificado, sustituir id por el deseado</p>
<p><a href="/api/indicadores">/api/indicadores</a>;: Obtener todos los indicadores.</p>
<p><a href="/api/indicadores/1008000012">/api/indicadores/$id</a>;: Obtener indicador con el id especificado, sustituir id por el deseado</p>
<p><a href="/api/temas-nivel-1">/api/temas-nivel-1&quot</a>: Obtener todos los temas de nivel 1</p>
<p><a href="/api/temas-nivel-1/0">/api/temas-nivel-1/$id</a>;: Obtener todos el tema nivel 1 con el id especificado, sustituir id por el deseado</p>
<p><a href="/api/temas-nivel-2">/api/temas-nivel-2</a>;: Obtener todos los temas de nivel 2</p>
<p><a href="/api/temas-nivel-2/0">/api/temas-nivel-2/$id</a>;: Obtener el tema nivel 2 con el id especificado, sustituir id por el deseado</p>
<p><a href="/api/temas-nivel-3">/api/temas-nivel-3</a>;: Obtener todos los temas de nivel 3</p>
<p><a href="/api/temas-nivel-3/0">/api/temas-nivel-3/$id</a>: Obtener todos los temas de nivel 3 con el id especificado, sustituir id por el deseado</p>
<p><a href="/api/indicadores-monto">/api/indicadores-monto</a>;: Obtener todos los registros de la tabla indicadores-monto</p>
<p><a href="/api/indicadores-monto/clave/1">/api/indicadores-monto/clave/$cl</a>; obtener el registro de indicadores-monto con la clave especificada, sustituir id por el deseado</p>
<p><a href="/api/indicadores-monto/entidades/0">/api/indicadores-monto/entidades/$entidad</a>; obtener el registro indicadores-monto de la entidad especificada, sustituir id por el deseado</p>
<p><a href="/api/indicadores-monto/municipios/0">/api/indicadores-monto/municipios/$municipio</a>; obtener el registro de indicadores-monto del municipio especificado, sustituir id por el deseado</p>
<p><a href="/api/indicadores-monto/clave-tema-nivel-1/0">/api/indicadores-monto/clave-tema-nivel-1/$n1</a>; obtener el registro de indicadores-monto del tema nivel 1 especificado, sustituir id por el deseado</p>
<p><a href="/api/indicadores-monto/clave-tema-nivel-2/0">/api/indicadores-monto/clave-tema-nivel-2/$n2</a>; obtener el registro de indicadores-monto del tema-nivel 2 especificado, sustituir id por el deseado</p>
<p><a href="/api/indicadores-monto/clave-tema-nivel-3/0">/api/indicadores-monto/clave-tema-nivel-3/$n3</a>; obtener el registro de indicadores-monto del tema-nivel 3 especificado, sustituir id por el deseado</p>
<p><a href="/api/indicadores-monto/anio/2001">/api/indicadores-monto/anio/$anio</a>; obtener los registros de indicadores-monto del anio especificado, sustituir por el anio deseado</p>
<p>&nbsp;</p>
</div>


<div class='bg-info'>
<p><center class='bg-primary'>MÉTODOS POST y PUT</center></p>

<p>&quot;/api/entidades/&quot;: insertar o actualizar una entidad</p>
<p>&quot;/api/municipios/&quot;: insertar o actualizar un  municipio</p>
<p>&quot;/api/indicadores/&quot;: insertar o actualizar un indicador</p>
<p>&quot;/api/indicadores-monto/&quot; insertar o actualizar indicador-monto</p>

<p>&nbsp;</p>
</div>

<div class='bg-info'>
<p><center class='bg-primary'>MÉTODOS DELETE</center></p>


<p>&quot;/api/entidades/$id&quot;: Eliminar la entidad con el id específicado</p>
<p>&quot;/api/municipios/$id&quot;: Eliminar el municipio con el id especificado</p>
<p>&quot;/api/indicadores/$id&quot;: Eliminar indicador con el id especificado</p>
<p>&quot;/api/temas-nivel-1/$id&quot;: Eliminar todos el tema nivel 1 con el id especificado</p>
<p>&quot;/api/temas-nivel-2/$id&quot;: Eliminar el tema nivel 2 con el id especificado</p>
<p>&quot;/api/temas-nivel-3/$id&quot;: Eliminar todos los temas de nivel 3 con el id especificado</p>
<p>&nbsp;</p>
</div>

</div>
</body>
</html>
