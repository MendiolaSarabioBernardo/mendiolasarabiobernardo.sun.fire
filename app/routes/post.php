<?php
//insercion de un registro de entidades
$app->post('/entidades/', function($id,$descripcion) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');

  $entidades = $app->db->prepare("insert into entidad values((:id_entidad),(':descripcion')");
$entidad->bindParam(':id_entidad',$id);
$entidad->bindParam(':descripcion',$descripcion);

 if ($entidad->execute() === false) {
   
    $app->halt(500, "Error: No se ha podido borrar la entidad con id '$id'.");
  }

  if ($entidad->rowCount() != 1) {
   
    $app->halt(404, "Error: La entidad con id '$id' no existe.");
  }

  /* En caso de exito en el borrado del libro en la bd, se le indica al cliente */
  $app->halt(200, "Exito: La entidad con id '$id' ha sido borrada");
});





